# pyimc

> **3TH ANNIVERSARY & MAINTAINCE STOPPED**
>  
> Thanks pyimcd for 3 years stable running.
>  
> If you want to continue maintaince pyimc, please fork (in GitLab) or clone this repo to other platform (GitHub etc.)
>  
> The only restriction is keep GPLv3 license.

A python library provides PythonAPI to log in to the iMC Portal. Tested on SIE's Campus Network.

Login to the iMC Portal in python!

## Additional info

If your Campus Network need iNode 802.1x authentication, we recommend the [KiritoA/c3h_client](https://github.com/KiritoA/c3h_client).

`pyimc` **can't** bypass any restriction. At least, you need a **valid** campus network account.

## TODO

[ ] write `setup.py`

## CLI

Run `pipenv install` to install dependency first.

Command | Description
:-: | :-
`pipenv run cli` | Run pyimc's example CLI program.
`pipenv run pyimcd` | Run pyimcd.
`pipenv run decode` | Active a interaction CLI to decode imc_pl (For develop only)
`pipenv run clean` | Clean PyInstaller build files
`pipenv run clean` | Clean all cache file & build dist

## Systemd service

*Please read [pyimcd.md](pyimcd.md)*

## Python Interface Example

```Python
import pyimc

pc = pyimc.PortalClient('2019000001@s.month','Password','127.0.0.1')
    try:
        pc.login()
    except pyimc.ImcError as e:
        raise RuntimeError(e.msg)
    if pc.isLoggedIn:
        time.sleep(10)
        pc.sentHeartBeat()
        pc.loopHeartBeat()
```

Need more example? You may want to read [`imc-cli.py`](imc-cli.py) and [`pyimcd.py`](pyimcd.py).

## License

```plaintext
    Copyright (C) 2020  OpenSIE

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

```

In a nutshell, **USE AS YOUR OWN RISK**

## Inspire from

- [Besfim/iMC-Portal-Login](https://github.com/Besfim/iMC-Portal-Login)
- <https://blog.meetwhy.com/blog/iMCPortal.html>
