# pyimcd Installation document

## Install

First, install all deps and build binary file.

```shell
pipenv install --dev
./pyimcd-install.sh
```

Second, install the systemd service unit file.

```shell
sudo install -Dm644 pyimcd@.service /usr/lib/systemd/system/pyimcd@.service
sudo systemctl daemon-reload
```

Third, put config json file into `/etc/pyimcd`. *For config file example, see [config-example.json](config-example.json)*

Check your service before start.

```shell
sudo systemctl status pyimcd@<your config name>

sudo systemctl start pyimcd@<your config name>
```

For example, if your config file is `/etc/pyimcd/test1.json`

```shell
sudo systemctl status pyimcd@test1

sudo systemctl start pyimcd@test1
```

## Uninstall

```shell
sudo rm /usr/bin/pyimcd
sudo rm /usr/lib/systemd/system/pyimcd@.service
sudo systemctl daemon-reload
```
