#!/bin/python3
"A command line client for H3C iMC Portal gatway"

'''
    Copyright (C) 2020  OpenSIE

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import click, json, time

import pyimc

from pyimc.date_structure import ImcError

portalIP = "59.73.16.70"
G_account = ""
G_password = ""
G_prefix = ""


@click.group()
@click.option('-cip', "--client-ip", type=str)
@click.option('-pip', "--portal-ip", type=str)
@click.option('-p',
              "--prefix",
              type=str,
              default="portal",
              help="prefix in the portal URL")
@click.option('-c',
              '--config',
              type=click.File('r'),
              help="Filepath of the config file which written in json.")
def cli(client_ip, portal_ip, config, prefix):
    global portalIP, G_account, G_password, G_prefix
    if config:
        ccc = json.loads(config.read())
        G_account = ccc.get('account')
        G_password = ccc.get('password')
        G_prefix = ccc.get('prefix','portal')
    else:
        ccc = {}

    if prefix != 'portal' and prefix:
        G_prefix = prefix

    pd = pyimc.PortalDetector()
    if pd.isPortal == False:
        print("WARNING: Can't detect Portal, you may not need to login.")
    if portal_ip:
        portalIP = portal_ip
    elif ccc.get('portalIP'):
        portalIP = ccc.get('portalIP')
    elif pd.isPortal:
        portalIP = pd.getPortalIP()
        print("INFO: Auto detect the portal IP: " + portalIP)


@cli.command()
@click.argument('account', required=False)
@click.argument('password', required=False)
def login(account, password):
    "Start login and enter heartbeat loop."
    #LOAD CONFIG
    if G_account and account == None:
        account = G_account
    if G_password and password == None:
        password = G_password
    if account == None or password == None:
        print("account or password is None, please check again.")
        print(f"account: {account}\npassword: {password}")
        return

    pc = pyimc.PortalClient(account, password, portalIP, prefix=G_prefix)
    #time.sleep(5)
    try:
        pc.login()
    except ImcError as e:
        #print("iMC ERROR: %s" % e.msg)
        raise RuntimeError(e.msg)
    if pc.isLoggedIn:
        time.sleep(10)
        pc.sentHeartBeat()
        pc.loopHeartBeat()


@cli.command()
@click.argument('account', required=False)
@click.argument('password', required=False)
def logout(account, password):
    "Logout your account"
    #Load config
    if G_account and account == None:
        account = G_account
    if G_password and password == None:
        password = G_password
    if account == None or password == None:
        print("account or password is None, please check again.")
        print(f"account: {account}\npassword: {password}")
        return

    pc = pyimc.PortalClient(account, password, portalIP, prefix=G_prefix)
    pc.logout()


@cli.command()
def cip():
    "Get client IP(test only)"
    print(pyimc.PortalClient.getClientIP())


@cli.command()
def detect():
    "Detect the portal, if true, show the IP address of the portal server."
    pd = pyimc.PortalDetector()
    print(f"isPortal: {pd.isPortal}")
    pIP = pd.getPortalIP()
    if pIP:
        print("Portal IP prob: " + pIP)


if __name__ == "__main__":
    cli()  #pylint: disable=no-value-for-parameter
