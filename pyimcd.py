#!/bin/python3
"Use pyimc as systemd service."
'''
    Copyright (C) 2020  OpenSIE

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import json
import logging
import logging.handlers
import signal
import time
from sys import exit # For PyInstaller exec. pylint: disable=redefined-builtin

import click

import pyimc
from pyimc.date_structure import ImcError

pc = object


def handle_exit(sig, frame):
    global pc
    print('SIGTERM Received! Try to logout.')
    pc.logout()
    exit()


def getConfigDict(s: str, isWatch: bool):
    """
    s - Json str
    isWatch - if true, program will wait until discovered the portal IP.
    
    If no portal IP configured, try Auto discovered."""
    j = json.loads(s)
    try:
        logging.debug("protal ip: %s", j['portalIP'])
    except KeyError:
        """
        pIP = autoDiscoverPortalIP()
        if pIP:
            j.update({'portalIP': pIP})
        else:
            exit(0)
        """
        while True:
            pIP = autoDiscoverPortalIP()
            if pIP:
                j.update({'portalIP': pIP})
                break
            else:
                if not isWatch:
                    exit(0)
                print("Waiting 30 second...")
                time.sleep(30)
    return j


def autoDiscoverPortalIP():
    print('Start to discover the Portal IP...')
    pd = pyimc.PortalDetector()
    if pd.isPortal:
        r = pd.getPortalIP()
        print(f'Auto discovered the Portal IP: {r}')
        if not r:
            print("Fail to find portal IP addr in the respond!")
            print(f"""===== DEBUG INFO =====
            r.text = {pd.r.text}
            r.url = {pd.r.url} 
            """)
        return r
    else:
        print("WARNING: Can't detect Portal, you may not need to login.")
        return None


def setFileHandler(filepath, p: pyimc.PortalClient, log_level='DEBUG'):
    fH = logging.handlers.RotatingFileHandler(filepath,
                                              maxBytes=4 * 1024 * 1024,
                                              backupCount=3,
                                              encoding='UTF-8')
    fH.setLevel(log_level)
    ff = logging.Formatter("[{asctime}][{module}][{levelname}]: {message}",
                           datefmt="%Y-%m-%d %H:%M:%S")
    fH.setFormatter(ff)
    p.logger.addHandler(fH)


@click.command()
@click.argument('config', type=click.File('r', encoding='UTF-8'))
@click.option('-f', '--log', type=str, help="Filepath to log file")
@click.option('-d',
              '--log-level',
              type=click.Choice(
                  ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG'],
                  case_sensitive=False),
              default='INFO')
@click.option('--watch/--no-watch',
              '-w',
              default=False,
              help="Watch until the portal can be detect.")
def main(config, log, log_level, watch):
    """
    This script used to run pyimc as simple systemd service. This script can read config from json file and provide simple command line interface for config log file. 

    You can run this python script with `pipenv run pyimcd`
    """
    global pc
    cd = getConfigDict(config.read(), watch)
    pc = pyimc.PortalClient(cd['account'],
                            cd['password'],
                            cd['portalIP'],
                            cd.get('prefix', 'portal'),
                            loggerLevel=log_level)
    signal.signal(signal.SIGTERM, handle_exit)
    if log:
        setFileHandler(log, pc, log_level)
    try:
        pc.login()
        if pc.isLoggedIn:
            i = 1
            while True:
                time.sleep(850)
                pc.sentHeartBeat()
                print(f"Heartbeat index [{i}]")
                i += 1
        else:
            print('login failed')
            exit(1)
    except ImcError as e:
        #print("iMC ERROR: %s" % e.msg)
        raise RuntimeError(e.msg)
    except KeyboardInterrupt: # FIXME: PyInstaller may not handle Ctrl+C correctly
        print('KeyboardInterrupt Received! Try to logout.')
        pc.logout()
        exit()


# TODO: Write self daemon function.

if __name__ == "__main__":
    main()  #pylint: disable=no-value-for-parameter
