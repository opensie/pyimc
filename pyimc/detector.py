#!/bin/python3
# A simple portal detector in Python

import re

import requests


class PortalDetector():
    "Detect the portal server by connect to the firefox's server"

    def __init__(self):
        self.r = requests.get("http://detectportal.firefox.com/success.txt",
                              timeout=10)
        self.r.raise_for_status()
        if self.r.text == "success\n":
            self.isPortal = False
        else:
            self.isPortal = True

    def getPortalIP(self):
        "Try to discovery the IP address of the portal server, may not correctly"
        ipReg = re.compile(
            r"((2(5[0-5]|[0-4]\d))|[0-1]?\d{1,2})(\.((2(5[0-5]|[0-4]\d))|[0-1]?\d{1,2})){3}"  #pylint: disable=anomalous-backslash-in-string
        )
        if self.r.text != "":
            ipStr = self.r.text
        else:
            ipStr = self.r.url
        m = ipReg.search(ipStr)
        if m:
            return m.group(0)
        else:
            return None


if __name__ == "__main__":
    pd = PortalDetector()
    print(f"isPortal: {pd.isPortal}")
    pIP = pd.getPortalIP()
    if pIP:
        print("Portal IP prob: " + pIP)
