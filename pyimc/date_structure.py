"Date structure about pl"

import base64
import json
import logging
import urllib.parse

from .utils import fix_padding


class ImcPL():
    def __init__(self, raw_pl: str):
        """
        A human friendly interface to read/resolve imc_pl.

        just like a dict object!
        
        Args:

            raw_pl (str): raw imc_pl from the portal server.
        """
        if raw_pl == "test-dev-only":
            return
        #Logging
        self.logger = logging.getLogger("PortalClient.ImcPL")
        #Start
        self.raw_pl = raw_pl
        dPL = self.decodePL(raw_pl)
        self.decoded_pl = dPL
        #show debug
        self.logger.info("imc_pl updated")
        self.logger.debug(json.dumps(dPL, indent=4, ensure_ascii=False))
        #Parse
        #self.errorNumber=""
        self.errorNumber = int(dPL['errorNumber'])
        """
        try:
            self.errorCode = dPL['portServIncludeFailedCode']
            self.msg = dPL['portServIncludeFailedReason']
        except KeyError as e:
            self.logger.warning(e)
            self.errorCode = None
            self.msg = None
        """

    def __getitem__(self, key):
        try:
            r = self.decoded_pl[key]
        except KeyError:
            self.logger.warning(
                f"Can't find ['{key}'] in pl, it will be replace by None.")
            return None
        else:
            return r

    @staticmethod
    def decodePL(pl: str):
        """
        Decode imc_pl
        
        Args:
            
            pl (str): raw imc_pl from the portal server.
        
        Returns:
            
            dict: Contain the key-value in json
        """
        jText = urllib.parse.unquote(
            str(base64.b64decode(fix_padding(pl), validate=False),
                encoding='UTF-8'))
        r = json.loads(jText)
        return r


class ImcError(Exception):
    def __init__(self, pld: ImcPL):
        """
        A custom exception class contain the errorCode and msg in ImcPL.
        
        Args:
            pld (ImcPL): decoded pl
        """
        self.errorCode = pld[pld['e_c']]
        self.msg = pld[pld['e_d']]


if __name__ == "__main__":
    print('Time to decode some pl.')
    while True:
        sss = input('pl-decode>')
        if sss:
            r = ImcPL.decodePL(sss)
            print(json.dumps(r, indent=4, ensure_ascii=False))
