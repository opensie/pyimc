#iMC Portal in Python
import base64
import json
import logging
import socket
import time

import requests

from .date_structure import ImcError, ImcPL
from .utils import prefixStandardizer


class PortalClient():
    def __init__(self,
                 account: str,
                 password: str,
                 portalIP,
                 prefix='portal',
                 loggerLevel=logging.DEBUG):
        """
        [summary]
        
        Args:
            
            account (str): Usually is your student number. (For example: 2019000001)
            
            password (str): Your account's password.
            
            portalIP (str): The IP addres of portal server.
            
            prefix (str, optional): Where to find: 'http://portalIP/*prefix*/......', Defaults to 'portal'.
            
            loggerLevel (optional): loggin's log level. Defaults to logging.DEBUG.
        """
        #Set vars
        self.account = account
        self.password = password
        self.isLoggedIn = False
        self.portalIP = portalIP
        self.clientIP = None
        self.portalPrefix = prefixStandardizer(prefix)
        self.imc_pl = ImcPL("test-dev-only")
        #Logging
        self.logger = logging.getLogger("PortalClient")
        self.logger.setLevel(logging.DEBUG)
        fmt = logging.Formatter(
            '[%(asctime)s][%(name)s][%(levelname)s]: %(message)s')
        hld = logging.StreamHandler()
        hld.setFormatter(fmt)
        hld.setLevel(loggerLevel)
        self.logger.addHandler(hld)
        self.logger.info("Logger has setup!")
        #INIT Session
        self.session = requests.Session()
        self.session.headers = {
            'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0',
            'Accept-Language':
            'zh-Hans-CN, zh-Hans; q=0.8, en-US; q=0.5, en; q=0.3',
            'Accept':
            'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Cache-Control': 'no-cache',
            "Connection": "keep-alive"
        }
        #INIT
        self.logger.info(
            f"Try to connect the Portal Server[{self.portalIP}]...")
        r1 = self.session.get(
            f"http://{portalIP}/{self.portalPrefix}index_default.jsp",
            timeout=10)
        r1.raise_for_status()
        #self.imc_pl = ImcPL(self.session.cookies.get('i_p_pl'))
        self.logger.debug(f"INIT Cookies: {self.session.cookies}")

    def login(self):
        "Start iMC Portal login"
        d = {
            "userName":
            f"{self.account}@s.month",
            "userPwd":
            str(base64.b64encode(bytes(self.password, encoding="UTF-8")),
                encoding='UTF-8'),  #FIXME: check this line
            "userDynamicPwd":
            "",
            "userDynamicPwdd":
            "",
            "serviceType":
            "s.month",
            "isSavePwd":
            "on",
            "userurl":
            None,  #http://detectportal.firefox.com/success.txt
            "userip":
            None,
            "basip":
            "",
            "language":
            "Chinese",
            "usermac":
            "null",
            "wlannasid":
            "",
            "wlanssid":
            "",
            "entrance":
            "null",
            "loginVerifyCode":
            "",
            "userDynamicPwddd":
            "",
            "customPageId":
            "0",
            "pwdMode":
            "0",
            "portalProxyIP":
            "59.73.16.70",
            "portalProxyPort":
            "50200",
            "dcPwdNeedEncrypt":
            "1",
            "assignIpType":
            "0",
            "appRootUrl":
            "",
            "manualUrl":
            "",
            "manualUrlEncryptKey":
            ""
        }
        """
        customHead = self.session.headers
        customHead.update({
            'X-Requested-With':
            'XMLHttpRequest',
            'Content-Type':
            'application/x-www-form-urlencoded; charset=UTF-8'
        })
        """
        self.logger.info(f"Start iMC Portal Login with [{self.account}]")
        self.logger.debug(f"POST Payload: {d}")
        req = self.session.post(
            f"http://{self.portalIP}/{self.portalPrefix}pws?t=li",  #I'M STUPID
            d,
            timeout=30)
        self.logger.debug("pl=" + req.text)
        self.logger.debug(f"HTTP Status: {req.status_code}")
        #self.logger.debug(f"Headers: {self.session.headers}")
        #self.logger.debug(f'Session Cookies: {self.session.cookies}')
        self.imc_pl = ImcPL(req.text)
        if self.imc_pl.errorNumber == 1:
            self.imc_pl = ImcPL(self.imc_pl['portalLink'])
            #FIXME: May not need
            p = {
                'pl': self.imc_pl.raw_pl,
                'custompath': None,
                'uamInitCustom': '1',
                'uamInitLogo': 'H3C',
                'customCfg': 'LTE',
                'loginType': '3',
                'v_is_selfLogin': '0',
                'byodserverip': '0.0.0.0',
                'byodserverhttpport': '80'
            }
            t2 = self.session.get(
                f"http://{self.portalIP}/{self.portalPrefix}page/afterLogin.jsp",
                params=p,
                timeout=30)
            #t2.raise_for_status()
            #Finish
            self.logger.info('Successful login, congratulate!')
            self.isLoggedIn = True
        else:
            raise ImcError(self.imc_pl)  # TODO: EXCEPTION HANDLER

    def logout(self):
        "Logout your account"
        #Build parms
        p = {'pl': self.imc_pl.raw_pl, 'hlo': None}
        self.logger.info("Start Logout")
        self.logger.debug("parms: %s" % json.dumps(p, indent=4))
        t3 = self.session.post(
            f"http://{self.portalIP}/{self.portalPrefix}page/logout.jsp",
            params=p,
            timeout=30)
        t3.raise_for_status()
        p = {
            't': 'lo',
            'language': 'Chinese',
            'userip': self.clientIP,  # May not need
            'basip': None,
            '_': int(time.time() * 1000)  #Time stamp
        }
        t4 = self.session.get(f"http://{self.portalIP}/{self.portalPrefix}pws",
                              timeout=30,
                              params=p)
        t4.raise_for_status()
        self.imc_pl = ImcPL(t4.text)
        if self.imc_pl.errorNumber == 1:
            self.isLoggedIn = False
            self.logger.info('Successful logout.')
        else:
            raise ImcError(self.imc_pl)

    @staticmethod
    def logoutStatic(portalIP, portalPrefix):
        "Logout without login, just like click logout button on web UI.(UNDER DEVELOPING...)"
        #TODO: Implation static logout method. By review network traffic when click logout button.
        headers = {
            "User-Agent":
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0"
        }
        payload = json.loads("""{
	"userName": "",
	"userPwd": "",
	"userDynamicPwd": "",
	"userDynamicPwdd": "",
	"serviceType": "s.month",
	"userurl": "",
	"userip": "",
	"basip": "",
	"language": "Chinese",
	"usermac": "null",
	"wlannasid": "",
	"wlanssid": "",
	"entrance": "null",
	"loginVerifyCode": "",
	"userDynamicPwddd": "",
	"customPageId": "0",
	"pwdMode": "0",
	"portalProxyIP": "59.73.16.70",
	"portalProxyPort": "50200",
	"dcPwdNeedEncrypt": "1",
	"assignIpType": "0",
	"appRootUrl": "http://59.73.16.70/portal/",
	"manualUrl": "",
	"manualUrlEncryptKey": ""}""")
        req = requests.post(f"http://{portalIP}/{portalPrefix}pws?t=lo",
                            payload,
                            headers=headers,
                            timeout=30)
        reqPL = ImcPL(req.text)
        print(f"[Static LOGOUT] return: {reqPL.decoded_pl}")
        req.raise_for_status()

    def sentHeartBeat(self):
        "sent a heart beat package"
        #Build payload
        p = {
            "userip": self.clientIP,
            "basip": "",
            "userDevPort":
            self.imc_pl['userDevPort'],  #"SR6602-vlan-00-0000@vlan"
            "userStatus": self.imc_pl['userStatus'],
            "serialNo": self.imc_pl['serialNo'],
            "language": "Chinese",
            "e_d": "",
            "t": "hb"
        }
        self.logger.info("Senting heartbeat package...")
        self.logger.debug("payload:" + json.dumps(p, indent=4))
        #POST
        t5 = self.session.post(
            f'http://{self.portalIP}/{self.portalPrefix}page/doHeartBeat.jsp',
            data=p,
            timeout=30)
        if t5.status_code == 200:
            self.logger.info("Heartbeat package sent successful!(200)")
        else:
            self.logger.error(f"Heartbeat package sent fail({t5.status_code})")
            self.logger.debug("Server resopen: %s" % t5.text)
            t5.raise_for_status()

        #TODO: other exception handel

    def loopHeartBeat(self):
        "A sample loop for sent heartbeat package, 15min a time."
        try:
            while True:
                time.sleep(850)
                self.sentHeartBeat()
        except KeyboardInterrupt:
            self.logger.info(
                "KeyboardInterrupt detected, try to logout current account.")
        finally:
            self.logout()

    @staticmethod
    def getClientIP():
        #TODO: use netifaces to rewrite this function
        # May not needed
        hostname = socket.gethostname()
        ip = socket.gethostbyname(hostname)
        return ip
