#!/bin/python3
"Some useful functions"


def fix_padding(string):
    # via: https://stackoverflow.com/questions/2941995/python-ignore-incorrect-padding-error-when-base64-decoding
    string += '=' * (-len(string) % 4)
    return string


def prefixStandardizer(p: str) -> str:
    """
    /f/ - > f/
    
    Args:

        p (str): Input str
    
    Returns:

        str: Formatted str
    """
    if p[0] == '/':
        p = p[1:len(p) - 1]
    if p[len(p) - 1] != '/':
        p += '/'
    return p


if __name__ == "__main__":
    print(prefixStandardizer(input('prefix>')))