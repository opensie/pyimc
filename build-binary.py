import os
import platform

import click
import PyInstaller.__main__

if platform.os.name == 'nt':
    split_prefix = ';'
else:
    split_prefix = ':'


@click.command()
@click.argument('name')
def build(name):
    "Build static binary by name."
    PyInstaller.__main__.run(
        [f'--name={name}', '--onefile',
         os.path.abspath(f'{name}.py')])


if __name__ == "__main__":
    build()  #pylint: disable=no-value-for-parameter
